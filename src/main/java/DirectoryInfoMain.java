import org.apache.commons.io.FileUtils;

/**
 * 12 / 12 Punkte!
 *
 * Sehr gute Arbeit!
 */
public class DirectoryInfoMain {
    public static void main(String[] args) {
        System.out.println("Temp-Directory: " + FileUtils.getTempDirectoryPath());
        System.out.println("User Directory: " + FileUtils.getUserDirectoryPath());
    }
}
